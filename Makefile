install:
	git checkout .
	git pull
	sudo rm /etc/update-motd.d/00-hostname
	sudo rm /etc/update-motd.d/10-banner
	sudo rm /etc/update-motd.d/20-sysinfo
	sudo rm /etc/update-motd.d/colors
	sudo ln -s ~/motd/modules/00-hostname /etc/update-motd.d/00-hostname
	sudo ln -s ~/motd/modules/10-banner /etc/update-motd.d/10-banner
	sudo ln -s ~/motd/modules/20-sysinfo /etc/update-motd.d/20-sysinfo
	sudo ln -s ~/motd/modules/colors /etc/update-motd.d/colors
	sudo chmod +x /etc/update-motd.d/*
	ls /etc/update-motd.d/
	run-parts /etc/update-motd.d
