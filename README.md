# MOTD

> MOTD is the abbreviation of “Message Of The Day”, and it is used to display a message when a remote user login to the Linux Operating system using SSH. Linux administrators often need to display different messages on the login of the user, like displaying custom information about the server or any necessary information. To show custom MOTD, this post will guide you on how to show MOTD in Linux.
> From <https://linuxhint.com/show-motd-in-linux/>

## Setup

```bash
git clone https://gitlab.com/ewilan-riviere/motd.git ~/motd
```

### Init

```bash
make
```

### Install

```bash
sudo rm /etc/update-motd.d/00-hostname
sudo rm /etc/update-motd.d/10-banner
sudo rm /etc/update-motd.d/20-sysinfo
sudo rm /etc/update-motd.d/colors
```

```bash
sudo ln -s ~/motd/modules/00-hostname /etc/update-motd.d/00-hostname
sudo ln -s ~/motd/modules/10-banner /etc/update-motd.d/10-banner
sudo ln -s ~/motd/modules/20-sysinfo /etc/update-motd.d/20-sysinfo
sudo ln -s ~/motd/modules/colors /etc/update-motd.d/colors
```

```bash
ls /etc/update-motd.d/
```

```bash
sudo chmod +x /etc/update-motd.d/*
```

```bash
run-parts /etc/update-motd.d
```

## Example

From <https://github.com/bcyran/fancy-motd>
